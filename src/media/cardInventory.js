export default function cardInventory(){
    const cards = [
        {
            name:"Mana Leak",
            imageUrl:"https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fstatic.starcitygames.com%2Fsales%2Fcardscans%2FMTG%2FDDN%2Fen%2Fnonfoil%2FManaLeak.jpg&f=1&nofb=1"
        },
        {
            name: "Fire Ice",
            imageUrl:"https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.ebayimg.com%2Fimages%2Fg%2F1iMAAOSwpRhdJp~C%2Fs-l640.png&f=1&nofb=1"
        }
    ]
    return cards;
}
