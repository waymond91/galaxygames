import React, {useState, useEffect} from "react";
import {
    Row,
    Card,
    Button,
    InputGroup,
    FormControl, Col,
    Container
} from "react-bootstrap";
import Deck from "../components/Deck";
import Loader from "react-loader-spinner";
import {Link} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

const mtg = require('mtgsdk');

let backgroundImgStyle = {
    color: "white",
    background: "radial-gradient(circle, rgba(250,255,147,1) 0%, rgba(85,34,4,1) 90%)",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    height: "100vh",
    padding: "0",
};

const loadingBackground = {
    background: "rgba(0,0,0,.5)",
    textAlign: "center",
    height: "100%"
}

const buttonStyle = {
    width:"40%",
}

const CardSearch = () => {
    const [searchName, setSearchName] = useState("");
    const [validSearch, setValidSearch] = useState(false);
    const [searchResults, setSearchResults] = useState([]);
    const [cards, setCards] = useState([]);
    const [searching, setSearching] = useState(false);
    const [complete, setComplete] = useState(false);
    const [index, setIndex] = useState(-1);
    const [timedOut, setTimedOut] = useState(false);
    const [mobile, setMobile] = useState(true);
    const [foundCard, setFoundCard] = useState("");
    const [resultsLength, setResultsLength] = useState(0);
    const [reload, setReload] = useState(false);
    const [card, setCard] = useState({});

    const handleForm = (e) => {
        setSearchName(e.target.value);
    }

    const handleKey = (e) => {
        if (e.key == 'Enter') {
            handleSearch();
        }
    }

    function deckCallback(childData) {
        if (childData === true) {
            setIndex(index - 1);
        }
        if (index === 0) {
            setReload(true);
            const timer = setTimeout(() => {
                loadDeck();
                setReload(false);
                clearTimeout(timer);

            }, 2000);
        }
    }

    useEffect(() => {
        if (complete & resultsLength > 0 & !reload) {
            setCard(cards[index]);
        }

    }, [index, reload])

    const handleSearch = (e) => {
        setSearching(true);
        setSearchResults([])
        const searchTimeout = 1500;
        let timer = setTimeout(timeoutHandler, searchTimeout)
        mtg.card.all({name: searchName, pageSize: 1})
            .on('data', card => {
                clearTimeout(timer);
                timer = setTimeout(timeoutHandler, searchTimeout);
                if (!complete) {
                    setFoundCard("Located: " + card.name)
                    setSearchResults(searchResults => [...searchResults, card])
                } else {
                    return (0)
                }
            })
    }

    function loadDeck() {
        let tempCards = searchResults.slice(0, 10).filter(checkImgUrl);
        setCards(tempCards);
        setResultsLength(tempCards.length);
        setIndex(tempCards.length - 1);
        setComplete(true);
    }

    useEffect(() => {
        if ((searchResults.length > 10 & !complete) | (timedOut & !complete)) {
            loadDeck();
        }
    }, [searchResults, timedOut])

    useEffect(() => {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            setMobile(true);
        } else {
            setMobile(false);
        }
    }, [])

    function timeoutHandler() {
        setTimedOut(true);
        console.log("Search timed out");
    }

    function checkImgUrl(result) {
        if ('imageUrl' in result) {
            return (result);
        }
    }

    function researchCallback() {
        window.location.reload();
    }

    function renderSearchBar() {
        return (
            <Row className='d-flex justify-content-center'>
                <Card style={{background: "rgba(0,0,0,0)", borderWidth: "0px", borderRadius: '0px', margin: '0px'}}>
                    <br/>
                    <h3
                        className="text-center"
                        style={{paddingTop: "1rem"}}>
                        <b>Multiverse Locator</b>
                    </h3>
                    <Card.Body>
                        <InputGroup className="mb-3">
                            <FormControl
                                aria-label="Default"
                                aria-describedby="inputGroup-sizing-default"
                                onChange={handleForm}
                                onKeyPress={handleKey}
                                placeholder="Enter a card name or name fragment..."
                            />
                        </InputGroup>
                        <Row className="m-auto align-self-center">
                            <Button size="lg" variant="danger" disabled={validSearch} onClick={handleSearch}>
                                Triangulate!</Button>
                        </Row>
                    </Card.Body>
                </Card>
            </Row>
        );
    }

    function renderLoading() {
        return (
            <div style={loadingBackground}>
                <Row className='d-flex justify-content-center'>
                    <Col xs={12}>
                        <Loader
                            style={{paddingTop: "25vh"}}
                            type="BallTriangle"
                            color="#ffffff"
                            height="20vh"
                            width="20vh"
                            timeout={5000} //5 secs
                        />
                        <p style={{paddingTop: "1rem", textAlign: "center"}}>{foundCard}</p>
                    </Col>
                </Row>
            </div>
        )
    }

    function renderMobileDeck() {
        return (
            <div className="text-center" style={{position: "relative"}}>
                <Container fluid>
                    <Row style={{zIndex:"2"}}className="d-flex justify-content-center">
                        <Deck cards={cards} mobile={mobile} callback={deckCallback}/>
                    </Row>
                    <Row>
                        <h5 style={{paddingTop: "1rem"}}><b>{card.name}</b>: {card.set}</h5>
                    </Row>
                    <Row className="d-flex justify-content-center" style={{marginTop:"63vh", zIndex:"1"}}>
                        <Col style={buttonStyle}>
                            <Button size="lg" onClick={researchCallback} style={{width:"100%",height:"100%"}} variant="dark">Search Again</Button>
                        </Col>
                        <Col style={{buttonStyle}}>
                            <Link style={{width:"100%"}}
                                to={{
                                pathname: '/mobileCard',
                                state: {
                                    card: card,
                                }
                            }}>
                                <Button size="lg" style={{width:"100%",height:"100%"}} variant="danger">Get Today</Button>
                            </Link>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }

    function renderPower() {
        if (card.power || card.toughness) {
            return (
                <p style={{textAlign: "right"}}>{card.power}/{card.toughness}</p>
            )
        }
    }

    function renderDesktopDeck() {
        return (
            <div className="text-center" style={{position: "relative"}}>
                <Container fluid>
                    <Row>
                        <Col md={6}>
                            <Row className="d-flex justify-content-center">
                                <Deck cards={cards} mobile={mobile} callback={deckCallback}/>
                            </Row>
                        </Col>
                        <Col md={6}>
                            <Card style={{
                                height: "auto",
                                background: "rgba(255,255,255,0.65)",
                                margin: "15%",
                            }} text="dark">
                                <Card.Header>
                                    <h2><b>{card.name}</b></h2>
                                </Card.Header>
                                <Card.Subtitle style={{margin: '10px', textAlign: "left"}}
                                               className="mb-2 text-muted">{card.setName}</Card.Subtitle>
                                <Card.Body style={{textAlign: "left"}}>
                                    <p>{card.types}</p>
                                    <p>{card.originalText}</p>
                                    <p><i>{card.flavor}</i></p>
                                    {renderPower()}
                                    <Link to={{
                                        pathname: '/demo',
                                        state: {
                                            card: card,
                                        }
                                    }}>
                                        <Button style={buttonStyle} size="lg" variant="danger">Get Today</Button><br/>
                                    </Link>
                                    <Button onClick={researchCallback} style={buttonStyle} size="lg" variant="dark">Back
                                        to Search</Button>
                                </Card.Body>
                            </Card>

                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }

    function renderDeck() {
        if (mobile) {
            return (
                renderMobileDeck()
            )
        } else {
            return (
                renderDesktopDeck()
            )
        }

    }

    function render() {
        if (complete) {
            return (
                renderDeck()
            )
        }
        if (!searching) {
            return (
                renderSearchBar()
            )
        } else {
            return (
                renderLoading()
            )
        }
    }

    return (
        <div style={backgroundImgStyle}>
            {render()}
        </div>
    );
}

export default CardSearch;
