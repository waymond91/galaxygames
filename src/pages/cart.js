import React, {useState, useEffect} from "react";
import {Jumbotron, Row, ListGroup, Col, Button} from "react-bootstrap";

const Cart = () => {
    let [lineItems, setLineItems] = useState([]);
    const imgStyle = {
        width:"100%",
        height:"auto",
        maxWidth:"200px"
    }

    let backgroundImgStyle = {
        background: "radial-gradient(circle, rgba(250,255,147,1) 0%, rgba(85,34,4,1) 90%)",
        backgroundAttachment: "fixed",
        height: "100vh",
    };

    let cardStyle = {
        background: "rgba(255,255,255,0.0)",
        padding: "10px",
        borderRadius: "10px",
        marginBottom:"1rem",
    }
    let subStyle = {
        background: "rgba(255,255,255,0.75)",
        padding: "10px",
        borderRadius: "10px",
        marginBottom:"1rem",
    }

    const handleRemoveItem = (e) =>{
        localStorage.removeItem('cart:'+e.target.id);
        updateLineItems();
        setMedia(renderMedia());
    }

    function updateLineItems(){
        setLineItems([])
        for (const key of Object.keys(localStorage)) {
            if (key.includes('cart:')) {
                let item = JSON.parse((localStorage.getItem(key)));
                setLineItems(lineItems => [...lineItems, item])
            }
        }
        console.log(lineItems);
    }

    function renderMedia(){
        return (
            lineItems.map(item => (
                    <ListGroup.Item style={subStyle}>
                        <Row>
                        <Col xs={3}>
                        <img src={item.imageUrl}
                             alt={item.name}
                             class="d-inline"
                             style={imgStyle}
                        />
                        </Col>
                        <Col xs={5}>
                            <h6><b>{item.name} -</b> {item.subName}</h6>
                            <p>Order Qty: {item.qty}</p>
                        </Col>
                        <Col xs={4}>
                            <Button id={item.key} variant="outline-danger" onClick={handleRemoveItem}>Remove</Button>
                        </Col>
                        </Row>
                    </ListGroup.Item>
            ))
        )
    }
    let [media, setMedia] = useState(renderMedia());

    useEffect(() => {
        updateLineItems();
        setMedia(renderMedia());
        console.log(media);
    }, []);

    return(
        <div style={backgroundImgStyle}>
            <Row className="m-auto align-self-center">
                <Row>
                    <h5 style={{paddingTop: "0.5rem", textAlign:"center"}}>Shopping Cart</h5>
                </Row>
            <ListGroup style={cardStyle}>
                {renderMedia()}
            </ListGroup>
            </Row>
        </div>
    )
};

export default  Cart;
